FROM ubuntu:latest
# change YOUR_APP_SERVER_VERSION to app version for exmaple 0.0.1
ARG APP_SERVER_VERSION="YOUR_APP_SERVER_VERSION" 
ARG APP_NAME="YOUR_APP_NAME"

WORKDIR /app

COPY bin/godot.linuxbsd.template_release.x86_64 $APP_NAME
# your server binary name need to be $APP_NAME_$APP_SERVER_VERSION.pck for example shooter-server_0.0.1.pck
COPY bin/$APP_NAME_$APP_SERVER_VERSION.pck $APP_NAME.pck

COPY bin/start_server.sh start_server.sh

RUN chmod +x start_server.sh

RUN pwd

RUN ls /app

CMD ["./start_server.sh"]
EXPOSE 18000/udp
