# Godot Game Server Docker Compilator

Example of creating docker image with compilation for latest stable Godot Engine version

1. Change at godot-srv-compilation/build-godot.sh argument GODOT_VERSION to latest stable branch
2. RUN compilation for godot server with godot-srv-compilation/build.sh
3. copy created binary godot-srv-compilation/bin/godot.linuxbsd.template_release.x86_64 to bin
4. add to bin directory your compiled game server binary Export PCK/ZIP with resources option "Export as dedicated server"
5. change APP_NAME to your app name (for example shooter_online_server), change your SERVER_VERSION to your game server version
6. login to your Registry v2
7. run build.sh
