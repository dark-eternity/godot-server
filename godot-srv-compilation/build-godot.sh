#!/bin/sh
GODOT_VERSION=4.2

docker build -t godot-ubuntu:$GODOT_VERSION .

id=$(docker create godot-ubuntu:$GODOT_VERSION)
docker cp $id:/app/godot/bin/ ./bin
docker rm -v $id